import os

if 'DROPBOX_ACCESS_TOKEN' not in os.environ:
    raise Exception('Dropbox access token not found in ENV vars DROPBOX_ACCESS_TOKEN')
DROPBOX_ACCESS_TOKEN = os.environ['DROPBOX_ACCESS_TOKEN']

if 'FILE_TO_BACKUP' not in os.environ:
    raise Exception('Filename of file to backup not found in ENV var FILE_TO_BACKUP')
FILE_TO_BACKUP = os.environ['FILE_TO_BACKUP']
