import dropbox
import config
from datetime import datetime


dbx = dropbox.Dropbox(config.DROPBOX_ACCESS_TOKEN)
print("Got access to dropbox")

timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
print(f'Backup /{config.FILE_TO_BACKUP} to ./filename - {config.FILE_TO_BACKUP}')

dbx.files_download_to_file(f"./{timestamp}__{config.FILE_TO_BACKUP}", f"/{config.FILE_TO_BACKUP}")
print('Backup done')
